# MVC con PHP

- Repositorio para desarrollar las clases del tema 2
- MVC
-

##  Instalación:

- Crear el fichero /etc/apache2/sites-available/examen1802.conf

```
<VirtualHost *:80>
    ServerName examen1802.local

    ServerAdmin webmaster@localhost
    DocumentRoot /home/alumno/examen1802/public


    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    <Directory /home/alumno/examen1802/public>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
    </Directory>

</VirtualHost>

```

- Activar el servicio y reiniciar apache:

```
$ sudo a2ensite examen1802
$ sudo service apache2 restart
```

- Editar el fichero /etc/hosts. Añade la línea:

```
127.0.0.1   examen1802.local
```


# Enunciado

- Lista de jugadores /jugador: 2P
- Paginación de la lista de jugadores: 1P
- Mostrar el nombre del puesto asociado en vez de su id: 1P
- Mostrar las fechas con formato día-mes-año: 1P
- Creación de un nuevo jugador: 1.5P
- Modificar el formulario para que el `id_puesto` sea un <select> de acuerdo a la tabla puestos.  1P.
- Añadir jugadores a la lista de titulares en sesión: /jugador/titular/{id}.
- Ver la lista de titulares si los hay: /jugador/titulares  1.5P.
- Quitar jugadores de la lista de titulares: /jugador/quitar/{id}  1P.




```sql
DROP DATABASE IF EXISTS examen1802;
CREATE DATABASE examen1802;
use examen1802;


CREATE TABLE puestos(
    id int auto_increment PRIMARY KEY,
    nombre VARCHAR(100)
);

CREATE TABLE jugadores(
    id int auto_increment PRIMARY KEY,
    nombre VARCHAR(100),
    nacimiento DATETIME,
    id_puesto int,
    index(id_puesto),
    foreign key (id_puesto) REFERENCES puestos(id)
);

INSERT INTO puestos(nombre) VALUES
('Portero'),('Defensa'),('Centrocampista'),('Delantero');

INSERT INTO jugadores(nombre, id_puesto, nacimiento)
VALUES
('De Gea', 1,  '1998-05-22'),
('Kepa', 1,  '1997-03-12'),
('Azpilicueta', 2,  '1995-7-08'),
('Sergio Ramos', 2,  '1998-05-22'),
('Jordi Alba', 2,  '1997-03-12'),
('Iñigo Martínez', 2,  '1995-7-08'),
('S. Roberto', 3,  '1997-03-12'),
('Isco', 3,  '1998-05-22'),
('Asensio', 3,  '1997-03-12'),
('Saúl', 3,  '1988-05-22'),
('Busquets', 3,  '1990-03-12'),
('Diego Costa', 4,  '1995-7-08'),
('Rodrigo', 4,  '1998-01-22'),
('Iago Aspas', 4,  '1995-7-08'),
('Suso', 4,  '1994-03-12'),
('Morata', 4,  '1989-02-19');
```
