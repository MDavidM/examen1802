<?php
namespace App\Controllers;

//require_once  '../app/models/ProductType.php';

use \App\Models\User;
/**
*
*/
class TitularController
{

    function __construct()
    {
        // echo "En LoginController";
    }


    public function index()
    {
        $pagesize = 4;
        $titulares = User::paginate($pagesize);
        $rowCount = User::rowCount();
        //$users = User::all();
        $pages = ceil($rowCount / $pagesize);
        if (isset($_REQUEST['page'])){
            $page = (integer) $_REQUEST['page'];
        }else{
            $page = 1;
        }


        if (isset($_SESSION['titular'])){
            $titular = $_SESSION['titular'];
        }else{
            $_SESSION['titular'] = [];
        }
        require "../app/views/titulares/index.php";
    }

    public function add($arguments)
    {
        $id = (int) $arguments[0];
        $jugador = User::find($id);

        if (isset($_SESSION['titular'][$jugador->id])) {
            $_SESSION['titular'][$jugador->id];
        }else{
            $_SESSION['titular'][$jugador->id] = $jugador;
        }
        require "../app/views/titulares/index.php";
    }

    public function quitar($arguments) {
        $id = (int) $arguments[0];
        $jugador = User::find($id);

    if (isset($_SESSION['titular'][$jugador->id])) {
        unset($_SESSION['titular'][$jugador->id]);
      }
      require "../app/views/titulares/index.php";
    }
}
