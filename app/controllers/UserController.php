<?php
namespace App\Controllers;

//require_once  '../app/models/User.php';
use \App\Models\Puestos;
use \App\Models\User;
/**
*
*/
class UserController
{

    function __construct()
    {

    }

    public function index()
    {
        $pagesize = 4;
        $jugadores = User::paginate($pagesize);
        $rowCount = User::rowCount();
        //$users = User::all();
        $pages = ceil($rowCount / $pagesize);
        if (isset($_REQUEST['page'])){
            $page = (integer) $_REQUEST['page'];
        }else{
            $page = 1;
        }
        require "../app/views/jugador/index.php";
    }

    public function create()
    {
        require "../app/views/jugador/create.php";
    }

    public function store()
    {
        $jugador = new User();
        $jugador->nombre = $_REQUEST['nombre'];
        $jugador->nacimiento = $_REQUEST['nacimiento'];
        $jugador->id_puesto = $_REQUEST['id_puesto'];
        $jugador->insert();
        header('Location:/jugador');
    }

    public function update()
    {
        $id = $_REQUEST['id'];
        $jugador = User::find($id);
        $jugador->name = $_REQUEST['name'];
        $jugador->nacimiento = $_REQUEST['nacimiento'];
        $jugador->id_puesto = $_REQUEST['id_puesto'];
        $jugador->save();
        header('Location:/jugador');
    }
}
