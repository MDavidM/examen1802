<head>
    <?php require "../app/views/parts/head.php" ?>
</head>
<body>
   <?php require "../app/views/parts/header.php" ?>
    <main role="main" class="container">
      <div class="starter-template">
        <h1>Titulares</h1>
        <table class="table table-striped">
            <thead>
            <th>Id</th>
            <th>Nombre</th>
            <th>Puesto</th>
            <th>Fecha Nacimiento</th>
            <th>Operaciones</th>
            </thead>
            <tbody>
                <?php foreach ($jugador as $titular): ?>
                    <tr>
                        <td><?php echo $jugador->id ?></td>
                        <td><?php echo $jugador->nombre ?></td>
                        <td><?php echo $jugador->id_puesto ?></td>
                        <td><?php echo $jugador->nacimiento->format('d-m-Y') ?>
                        <td><a class="btn btn-primary" href="titular/quitar/<?php echo $jugador->id ?>">Quitar Titular</td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
        <?php for ($i = 1; $i <= $pages; $i++) { ?>
            <?php if ($i != $page): ?>

            <a href="/user?page=<?php echo $i ?>" class="btn">
                <?php echo $i ?>
            </a>
        <?php else: ?>
            <span class="btn">
                <?php echo $i ?>
            </span>
         <?php endif?>
         <?php }?>
        <hr>
      </div>
    </main><!-- /.container -->
<?php require "../app/views/parts/footer.php" ?>
</body>
 <?php require "../app/views/parts/scripts.php" ?>
</html>
