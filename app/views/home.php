<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>

  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <div class="starter-template">
      <h1>Examen 1802: MVC</h1>

      <p class="lead">Página de presentación</p>

      <h2>Enunciado</h2>
      <pre>
- Lista de jugadores /jugador: 2P
- Paginación de la lista de jugadores: 1P
- Mostrar el nombre del puesto asociado en vez de su id: 1P
- Mostrar las fechas con formato día-mes-año: 1P
- Creación de un nuevo jugador: 1.5P
- Modificar el formulario para que el `id_puesto` sea un "select" de acuerdo a la tabla puestos.  1P.
- Añadir jugadores a la lista de titulares en sesión: /jugador/titular/{id}.
- Ver la lista de titulares si los hay: /jugador/titulares  1.5P.
- Quitar jugadores de la lista de titulares: /jugador/quitar/{id}  1P.

      </pre>

    </div>

  </main><!-- /.container -->
  <?php require "../app/views/parts/footer.php" ?>

</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
