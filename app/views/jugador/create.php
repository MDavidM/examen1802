<!doctype html>
<html lang="es">
<head>
  <?php require "../app/views/parts/head.php" ?>
</head>
<body>
  <?php require "../app/views/parts/header.php" ?>
    <main role="main" class="container">
      <div class="starter-template">
        <h1>Alta de jugadores</h1>
        <form method="post" action="/user/store">
            <div class="form-group">
                <label>Nombre:</label>
                <input type="text" class="form-control" name="nombre">
            </div>
            <div class="form-group">
                <label>Fecha Nacimiento:</label>
                <input type="text" class="form-control" name="nacimiento">
            </div>
             <div class="form-group">
                <label>Puesto:</label>
                <input type="text" class="form-control" name="id_puesto">
                <!--<select name="id_puesto">
                <?php foreach ($puestos as $puesto) {?>
                <option value="<?php echo $puesto->id?>">
                <?php echo $puesto->nombre ?>
              </option>
              <?php } ?>-->
             </div>
             <input type="submit" class="btn btn-default"><br>
        </form>
      </div>
    </main><!-- /.container -->
 <?php require "../app/views/parts/footer.php" ?>
</body>
  <?php require "../app/views/parts/scripts.php" ?>
</html>
