<head>
    <?php require "../app/views/parts/head.php" ?>
</head>
<body>
   <?php require "../app/views/parts/header.php" ?>
    <main role="main" class="container">
      <div class="starter-template">
        <h1>Lista de jugadores</h1>
        <table class="table table-striped">
            <thead>
            <th>Id</th>
            <th>Nombre</th>
            <th>Fecha Nacimiento</th>
            <th>Puesto</th>
            <th>Acciones</th>
            </thead>
            <tbody>
                <?php foreach ($jugadores as $jugador): ?>
                    <tr>
                        <td><?php echo $jugador->id ?></td>
                        <td><?php echo $jugador->nombre ?></td>
                        <td><?php echo $jugador->nacimiento->format('d-m-Y') ?>
                        <td><?php echo $jugador->id_puesto ?></td>
                        <td><a class="btn btn-primary" href="/titular/add/<?php echo $jugador->id ?>">Titular</td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
        <?php for ($i = 1; $i <= $pages; $i++) { ?>
            <?php if ($i != $page): ?>

            <a href="/user?page=<?php echo $i ?>" class="btn">
                <?php echo $i ?>
            </a>
        <?php else: ?>
            <span class="btn">
                <?php echo $i ?>
            </span>
         <?php endif?>
         <?php }?>
        <hr>
        <a href="/user/create">Nuevo</a>
      </div>
    </main><!-- /.container -->
<?php require "../app/views/parts/footer.php" ?>
</body>
 <?php require "../app/views/parts/scripts.php" ?>
</html>
