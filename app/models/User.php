<?php

namespace App\Models;

use PDO;
use Core\Model;
use App\Models\Puestos;

//require_once '../core/Model.php';
/**
*
*/
class User extends Model
{
    function __construct()
    {
        $this->nacimiento = new \DateTime($this->nacimiento);
    }

    public static function all()
    {
        $db = User::db();
        $statement = $db->query('SELECT * FROM jugadores');
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, User::class);

        return $jugadores;
    }

    public function paginate($size = 10)
    {
        if (isset($_REQUEST['page'])){
            $page = (integer) $_REQUEST['page'];
        }else{
            $page = 1;
        }

        $offset = ($page - 1) * $size;

        $db = User::db();
        $statement = $db->prepare('SELECT * FROM jugadores LIMIT :pagesize OFFSET :offset');
        $statement->bindValue(':pagesize', $size, PDO::PARAM_INT);
        $statement->bindValue(':offset', $offset, PDO::PARAM_INT);
        $statement->execute();

        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, User::class);

        return $jugadores;
    }

    public static function rowCount()
    {
        $db = User::db();
        $statement = $db->prepare('SELECT count(id) AS count FROM jugadores');
        $statement->execute();
        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);

        return $rowCount['count'];
    }

    public static function find($id)
    {
        $db = User::db();
        $statement = $db->prepare('SELECT * FROM jugadores WHERE id=:id');
        $statement->execute(array(':id' => $id));
        $statement->setFetchMode(PDO::FETCH_CLASS, User::class);
        $jugador = $statement->fetch(PDO::FETCH_CLASS);
        return $jugador;
    }

    public function insert()
    {
        $db = User::db();
        $statement = $db->prepare('INSERT INTO jugadores(nombre, nacimiento, id_puesto) VALUES(:nombre, :nacimiento, :id_puesto)');
        $statement->bindValue(':nombre', $this->nombre);
        $statement->bindValue(':nacimiento', $this->nacimiento);
        $statement->bindValue(':id_puesto', $this->id_puesto);
        return $statement->execute();
    }

    public function delete()
    {
        $db = User::db();
        $statement = $db->prepare('DELETE FROM jugadores WHERE id = :id');
        $statement->bindValue(':id', $this->id);
        return $statement->execute();
    }

    public function save()
    {
        $db = User::db();
        $statement = $db->prepare('UPDATE jugadores SET nombre = :nombre, nacimiento = :nacimiento, id_puesto = :id_puesto WHERE id = :id');
        var_dump($_REQUEST);
        $statement->bindValue(':id', $this->id);
        $statement->bindValue(':nombre', $this->name);
        $statement->bindValue(':nacimiento', $this->nacimiento);
        $statement->bindValue(':id_puesto', $this->id_puesto);
        return $statement->execute();
    }

    public function type()
    {
        //un producto pertenece a un tipo:
        $db = User::db();
        $statement = $db->prepare('SELECT * FROM puestos WHERE id = :id');
        $statement->bindValue(':id', $this->id_puesto);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_CLASS, Puesto::class);
        $type = $statement->fetch(PDO::FETCH_CLASS)/*[0]*/;

        return $type;
    }

    /*public function type()
    {
        //un producto pertenece a un tipo:
        $db = Product::db();
        $statement = $db->prepare('SELECT * FROM product_types WHERE id = :id');
        $statement->bindValue(':id', $this->type_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_CLASS, ProductType::class);
        $type = $statement->fetch(PDO::FETCH_CLASS);
        //[0]

        return $type;
    }
    */

}
