<?php
/**
*
*/
namespace App\Models;

use PDO;
use Core\Model;
use App\Models\User;

//require_once '../core/Model.php';
//require_once '../app/models/Product.php';
/**
*
*/
class ProductType extends Model
{

    function __construct()
    {

    }

    public function __get($atributoDesconocido)
    {
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
        } else {
            return "";
        }
    }

    public static function all()
    {
        $db = Puestos::db();
        $statement = $db->query('SELECT * FROM puestos');
        $all = $statement->fetchAll(PDO::FETCH_CLASS, Puestos::class);
        return $all;
    }

    public function jugadores()
    {
        $db = User::db();
        $statement = $db->prepare('SELECT * FROM jugadores WHERE id_puesto = :id');
        $statement->bindValue(':id', $this->id);
        $statement->execute();
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, Product::class);

        return $jugadores;

    }

    public function find($id)
    {
        $db = Puesto::db();
        $stmt = $db->prepare('SELECT * FROM puestos WHERE id=:id');
        $stmt->execute(array(':id' => $id));
        $stmt->setFetchMode(PDO::FETCH_CLASS, Puesto::class);
        $puesto = $stmt->fetch(PDO::FETCH_CLASS);

        return $puesto;
    }

        public function puesto()
        {
            //un producto pertenece a un tipo:
            $db = Product::db();
            $statement = $db->prepare('SELECT * FROM puestos WHERE id = :id');
            $statement->bindValue(':id', $this->id_puesto);
            $statement->execute();
            $statement->setFetchMode(PDO::FETCH_CLASS, puesto::class);
            $puestoid = $statement->fetch(PDO::FETCH_CLASS)/*[0]*/;

            return $puestoid;
        }

}
